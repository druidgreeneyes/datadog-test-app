FROM mcr.microsoft.com/dotnet/core/sdk:2.1-alpine AS build
WORKDIR /app

# copy csproj and restore as distinct layers
# COPY *.sln .
COPY ./*.csproj ./
RUN dotnet restore

# copy everything else and build app
COPY . .
WORKDIR /app
RUN dotnet publish -c Release -o out


FROM mcr.microsoft.com/dotnet/core/aspnet:2.1-alpine AS runtime
WORKDIR /app
COPY --from=build /app/out ./

RUN apk add libc6-compat

ADD misc/datadog-dotnet-apm-0.8.2.tar.gz /opt/datadog

ENTRYPOINT ["dotnet", "test.dll"]